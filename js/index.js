$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();        
    $('.carousel').carousel({
        interval: 3000
   });
    $('#contacto').on('show.bs.modal',function(e){
      console.log('El modal se esta mostrando');
      $('#contactoBtn').removeClass('btn-outline-success')
      $('#contactoBtn').addClass('btn')
      $('#contactoBtn').prop('disable',true)
    });
    $('#contacto').on('shown.bs.modal',function(e){
      console.log('El modal se mostro');
    });
    $('#contacto').on('hide.bs.modal',function(e){
      console.log('El modal se oculta');
    });
    $('#contacto').on('hidden.bs.modal',function(e){
      console.log('El modal se oculto');
      $('#contactoBtn').prop('disable',false)
      $('#contactoBtn').removeClass('btn')
      $('#contactoBtn').addClass('btn-outline-success')
    });
});